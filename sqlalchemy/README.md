# SqlAlchemy Module

## Purpose
To explain how to create, read, update, and delete from a database server using sqlalchemy. 
Also to explore relationships between entities, and how to make use of them to simplify code.
[tutorial](http://docs.sqlalchemy.org/en/latest/orm/tutorial.html)