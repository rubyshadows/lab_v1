# lab_v1

*Ordering the acquisition of knowledge stacks*

![alt text](https://render.fineartamerica.com/images/rendered/search/print/6.000/8.000/break/images-medium-5/glowing-dna-strand-johan-swanepoel.jpg "Knowledge Stack Analogy")

## Purpose
As human beings, we perform best when we strategize. Our lifespans are limited, so it's in our best interest to save time as we chase our dreams. Strategy accomplishes this.

This project is my learning strategy. It relies on testing and notetaking to **order** the knowledge I would like to possess.

## Principles 
Every Laboratory should be:
* Managed:
  1. Modular
  2. Locatable
  3. Re-engageable
  4. Accurately Categorized

* Tested:
  1. What was tested
  2. How was it tested

* Genuine:
  1. Private
  2. Progress Log
  3. Well Documented
  4. Useful

  Creating a laboratory may seem like a heavy task, just as creating any testing environment is, but after the environment is created then the actual testing will be easier, faster, and more effective than it was before the lab was in use.
