import unittest
import mymath
"""
    Running this module in interactive mode
    lets you experiment with test suites

    I mainly want to use tests to test units that
    are hard to run directly. Models. Engine components. etc.

    To do that:
        setup a test environment
        setup the situation right before the unit is used
        checkout the result.
"""

class TestAdd(unittest.TestCase):
    """
    Test the add function from the mymath library
    """
 
    def test_add_integers(self):
        """
        Test that the addition of two integers returns the correct total
        """
        result = mymath.add(1, 2)
        self.assertEqual(result, 3)
 
    def test_add_floats(self):
        """
        Test that the addition of two floats returns the correct result
        """
        result = mymath.add(10.5, 2)
        self.assertEqual(result, 12.4)
 
    def test_add_strings(self):
        """
        Test the addition of two strings returns the two string as one
        concatenated string
        """
        result = mymath.add('abc', 'def')
        self.assertEqual(result, 'abcdef')

    def pal():
        pass

if __name__ == '__main__':
    #unittest.main()
    x = unittest.TestSuite()
    x.addTest(TestAdd('test_add_floats'))
    y = unittest.TextTestRunner()
    z = y.run(x)