# Basic Unit Tests

## Purpose
Automated testing is valuable, and saves a lot of time in the long run, but it takes some precision to set up correctly. This module will help us work out that precision by focusing on one aspect of unit testing at a time.