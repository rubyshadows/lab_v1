'''This module experiments with testing under certain conditions'''
import unittest
import os
import random

def ran():
        x = random.randint(1,10)
        with open('outcome.txt', mode='a') as f:
            print(x, file=f)
        return x

class testA(unittest.TestCase):
    def test_1(self):
        pass
    
    @unittest.skipUnless(ran() % 2 == 0, "random is even, so skipping")
    def test_skip(Self):
        pass

class testB(unittest.TestCase):
    def test_1(self):
        pass

suiteA = unittest.TestSuite()
suiteB = unittest.TestSuite()

suiteA.addTest(testA('test_1'))
suiteB.addTest(testA('test_1'))

runner = unittest.TextTestRunner()

ran = random.randint(1,10)

if (ran % 2 == 0):
    x = runner.run(suiteA)
else:
    x = runner.run(suiteB)

def testerA():
    runner.run(testA('test_skip'))

def testerB():
    pass

# >>> [a for a in dict(vars(testA)) if a[0:4] == 'test']
# ['test_1']
# for loading a test suite automatically