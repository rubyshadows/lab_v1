# Awk Tests
1. Test whether actual output matches desired output
* List module's requirements in each test's comments
* Keep each test limited to the required context of the module being tested
* Find and document bugs (can module be ran multiple times safely?)
* Create each test right after creating its module
* Spend most time on buggiest modules

## List of awk tests
Empty