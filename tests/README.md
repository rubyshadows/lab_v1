# Tests
Package containing tests for the laboratory modules.


## Purpose
Ideally, the knowledge I've accumulated is reliable. Tests allow me to **measure** the reliability of my knowledge, by multiplying the quality of my tests with the amount of tests that my software can pass.

## Principles
1. Show presence of defects
2. Exhaustive testing is impossible, so prioritize
3. Test early to reduce cost of bug fixes
4. Focus on the problem modules
5. Update tests regularly to find more bugs
6. Tests are context dependant
7. The requirements themselves may not address business needs

* [Seven Software Testing Principles](https://www.youtube.com/watch?time_continue=350&v=NC1aqG4tWl4)
* [Software Testing Life Cycle (STLC)](https://www.youtube.com/watch?v=HylDB3bN6hQ)