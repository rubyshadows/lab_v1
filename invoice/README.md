#Invoice

##Purpose
This app allows for the creation of invoices.
It generates pdfs, and offers the option to have it emailed to you.
![Example](https://i.imgur.com/AgO2NhQ.png)