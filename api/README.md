# API
This tutorial will take us through the process of calling on an API, and creating an API.

## Calling an API
So long as the requester and responder headers agree, an API can be called from any platform:
* **Browser**
 * ![from browser](https://media.giphy.com/media/OOVyyi8f8SLN4GRB5W/giphy.gif)
* **Console**
 * ![from console](https://media.giphy.com/media/1r97fjuF2qRmmnGekH/giphy.gif)
* **Python**
 * ![from python](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSg-yQYUrvbarJVzF29Oq79iiw5ZnapTQ3ZoAKwH5v9EmILt2ty)
* **Javascript**
 * ![from javascript](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTpiVZegQoHI1cIlt_VsxA_EwI0gnc2l0KoCxJW-cb68UmOqdsujg)